console.log('[debug] ofify initialized');

module.exports = {
  install: (Vue, options) => {
    Vue.component('of-divider', () => import('./src/lib-components/Divider.vue'));
    Vue.component('of-spacer', () => import('./src/lib-components/Spacer.vue'));
    Vue.component('of-icon', () => import('./src/lib-components/Icon.vue'));
  }
}