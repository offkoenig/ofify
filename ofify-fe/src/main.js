import Vue from 'vue'
import App from './App.vue'

import Ofify from 'ofify';

Vue.use(Ofify)

Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
