# Ofify - Vue framework (Vue2)

### Components
* Horizontal divider with inset option
* Spacer for flex containers
* Parent-fit icon set (10+ ultr default icons (copy, cog-outline etc.))

### Installation

```sh
$ npm i ofify --save
```

### Usage
```javascript
import Ofify from 'ofify';
Vue.use(Ofify);
```

License
----

MIT
